import { EUser } from './../../models/User';
import { getConnection } from 'typeorm';
import { IAuth, IForm5Field } from '../../interfaces/attrReq';

export class MUser {

    public create5Form = (
        attr: IForm5Field
    ) => new Promise(async (resolve, reject) => {
        try {
            resolve({
                ...attr
            })
        } catch (error) {
            reject(error)
        }
    })

    public getByUsername = (
        username: string
    ): Promise<IAuth> => new Promise(async (resolve, reject) => {
        try {
            const users = await getConnection().getRepository(EUser).findOne({
                select: ["_id", "pid", "password", "role", "username"],
                where: {
                    username
                },
                relations: ["role"]
            })
            if (users === undefined) {
                reject("not found")
            } else {
                resolve({
                    hash: users.password,
                    pid: users.pid,
                    role: users.role.role,
                    username: users.username
                })
            }
        } catch (error) {
            reject(error)
        }
    })
    public getByPid = (pid: string, isAuthen = false): Promise<EUser | IAuth | any> => new Promise(async (resolve, reject) => {

        if (isAuthen) {

            try {
                const users = await getConnection().getRepository(EUser).findOne({
                    select: ["_id", "pid", "password", "role", "username"],
                    where: {
                        pid
                    },
                    relations: ["role"]
                })
                if (users === undefined) {
                    reject("not found")
                } else {
                    resolve({
                        hash: users.password,
                        pid: users.pid,
                        role: users.role.role,
                        username: users.username
                    })
                }
            } catch (error) {
                reject(error)
            }

        } else {
            try {
                const users = await getConnection().getRepository(EUser).findOne({
                    select: ["_id", "pid", "fname", "lname", "birthdate", "role", "username"],
                    where: {
                        pid
                    },
                    relations: ['role']
                })
                resolve({
                    ...users,
                    role: users.role.role
                })
            } catch (error) {
                reject(error)
            }
        }
    })
}