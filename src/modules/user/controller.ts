import { IForm5Field } from './../../interfaces/attrReq';
import { LOG } from './../../constants';
import { authorBy } from './../../utils/authorize';
import { MUser } from './model';
import { IReturn, IRequest, IResponse } from './../../interfaces/hapi';
import * as Boom from 'boom'
import { MLogging } from '../logging/model';


export class CUser {

    private model = new MUser();

    public sendForm = async (
        req: IRequest,
        h: IResponse
    ): Promise<IReturn | Boom> => {

        const doing = LOG.SEND5FORM.doing
        const token = req.headers.authorization
        let user: any
        if (token) {
            user = await authorBy(token)
            LOG.SEND5FORM.doing = LOG.SEND5FORM.doing + req.payload['pid']
            new MLogging().create(LOG.SEND5FORM, user)
            LOG.SEND5FORM.doing = doing
        } else {
            return Boom.unauthorized("Have no authorization token")
        }

        try {
            const results = await this.model.create5Form(req.payload as IForm5Field)
            return h.response({
                status: 201,
                message: "send form.",
                results
            })
        } catch (error) {
            return Boom.badRequest(error.message)
        }
    }
}
