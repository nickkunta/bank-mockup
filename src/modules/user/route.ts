import { adminValidation } from './../admin/validation';
import { CUser } from './controller';
import { API, HTTP_VERBS } from './../../constants';
import { IRoute } from './../../interfaces/hapi';
import { VUser } from './validation';



export const userRoutes: Array<IRoute> = [
    {
        path: `${API}/user/send`,
        method: HTTP_VERBS.POST,
        options: {
            auth: "jwt",
            plugins: {
                "hapiAuthorization": {
                    roles: ["user", "admin", "superAdmin"]
                }
            },
            validate: {
                payload: VUser.send5FormField.payload,
                headers: adminValidation.common.headers,
                options: {
                    allowUnknown: true
                }
            },
            tags: ['api', 'user'],
            handler: new CUser().sendForm
        }
    }
]