import * as Joi from 'joi'


export const authValidate = {

    login: {
        payload: {
            username: Joi.string().example("super-admin").required(),
            password: Joi.string().required().example("adminpw")
        }
    }

}