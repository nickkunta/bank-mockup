import { CLogging } from './controller';
import * as Joi from 'joi';
import { API, HTTP_VERBS } from './../../constants';
import { IRoute } from './../../interfaces/hapi';



export const logRoute: Array<IRoute> = [
    {
        path: `${API}/logging/{type?}`,
        method: HTTP_VERBS.GET,
        options: {
            auth: "jwt",
            validate: {
                headers: Joi.object({
                    Authorization: Joi.string().token().optional()
                }),
                options: {
                    allowUnknown: true
                },
                params: {
                    type: Joi.string().required().example("all")
                }
            },
            handler: new CLogging().getLog,
            tags: ["api", "log"]
        }
    }
]