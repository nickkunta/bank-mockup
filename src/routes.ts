import { IRoute } from './interfaces/hapi';
import { userRoutes } from './modules/user/route';
import { authRoute } from './modules/auth/route';
import { logRoute } from './modules/logging/route';
import { adminRoutes } from './modules/admin/route';



export const allRoutes: Array<IRoute> = [
    ...userRoutes,
    ...authRoute,
    ...logRoute,
    ...adminRoutes
]