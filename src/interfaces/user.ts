import { rolesType } from "../utils/enums";


export interface IUserJwt {
    role: rolesType
    pid: string
    username: string
}