import { rolesType } from "../utils/enums";

export interface IAdminCreate {
    fname: string,
    lname: string
    password: string,
    username: string
    pid: string
    role: any
}

export interface IUserEdit {
    pid: string
    name: string
    roles: rolesType
}


export interface IForm5Field {
    laserCode: string
    birthdate: string
    pid: string
    fname: string
    lname: string
}

export interface IAuth {
    hash: string
    pid: string
    role: rolesType
    username: string
}