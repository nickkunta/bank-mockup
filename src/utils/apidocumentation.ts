const descripApi = [
    {
        name: "session",
        description: "about session"
    }
];

const swaggerOptions = {
    info: {
        title: "Api for Bank Test",
        version: "1.0",
        description:
            "this is api reference use for know how to use api for bank only",
        contact: {
            name: "AliasName : azaz0133",
            email: "anirut.workspace@gmail.com"
        }
    },
    tags: descripApi,
    grouping: "tags"
};

export { swaggerOptions };
