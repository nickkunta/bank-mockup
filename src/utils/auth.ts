import { MUser } from './../modules/user/model';


export const authChecking = async (
    decoded,
    req
) => {
    try {
        const auth = await new MUser().getByPid(
            decoded["pid"]
        )
        if (auth === undefined) throw new Error("not found")

    } catch (error) {
        return {
            isValid: false
        }
    }
    return {
        isValid: true
    }
}