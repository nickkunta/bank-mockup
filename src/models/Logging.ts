import { EUser } from './User';
import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, ManyToOne } from "typeorm";


@Entity()
export class ELogging {

    @PrimaryGeneratedColumn()
    _id?: number;

    @ManyToOne(type => EUser, EU => EU.pid)
    who: EUser

    @Column()
    doing: string

    @Column()
    doingFlag: number


    @Column({
        default: true
    })
    useFlag?: boolean

    @CreateDateColumn()
    createdAt?: Date

    @UpdateDateColumn()
    updatedAt?: Date

}