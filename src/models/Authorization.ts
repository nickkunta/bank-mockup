import { EUser } from './User';
import { ROLES, rolesType } from './../utils/enums';
import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";


@Entity()
export class EAuthorization {
    @PrimaryGeneratedColumn()
    _id: number

    @Column({
        nullable: false,
        type: "enum",
        enum: ROLES
    })
    role: rolesType

    @OneToMany(type => EUser, EU => EU.pid)
    user: EUser
}